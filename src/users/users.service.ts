import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { Repository } from 'typeorm';
import { UserSingUpDto } from './dto/user-singup.dto';
import { UserSingInDto } from './dto/user-singin.dto';
import { sign } from 'jsonwebtoken';
import { hash,compare} from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UpdateUserDto } from './dto/user-update.dto';

@Injectable()
export class UsersService {

  constructor(@InjectRepository(UserEntity) private userRepository:Repository<UserEntity>,
  private jwtService:JwtService
  ){}
 
  //register
  async singup(userSingUpDto:UserSingUpDto):Promise<UserEntity>{
    //existe?
    const userExists= await this.findUserByEmail(userSingUpDto.email)
    if(userExists) throw new BadRequestException('Email is not avaliable')
    //hashea la contarsena
    userSingUpDto.password= await hash(userSingUpDto.password,10)//esto hashea la contrasena

    let user = this.userRepository.create(userSingUpDto);
    user= await this.userRepository.save(user)
    delete user.password
    //esto es para eliminar el password
    return user;
  }

  //login
  async singin(userSingInDto: UserSingInDto){
    //esto es para revisar si existe el email
    const userExists= await this.userRepository.createQueryBuilder('users').addSelect('users.password')
    .where('users.email=:email',{email:userSingInDto.email}).getOne()
    if(!userExists) throw new BadRequestException('Bad Creadentials.')
    //revisa la contrasena
    const matchPassword=await compare(userSingInDto.password,userExists.password)
    if(!matchPassword) throw new BadRequestException('Bad Creadentials.')
    //como en estas funcion se devuelven usuarios se elimina el campo que da la contrasena
    delete userExists.password

    //que ira encriptado
    const payload={id:userExists.id,name:userExists.name}
    //encripta
    const token = this.jwtService.sign(payload);
  

    const data ={
      user:userExists,
      token
    }
    return data;
  }

  async findAll() {
    return await this.userRepository.find();
  }
  async findOne(id: number):Promise<UserEntity>{
    const user= await this.userRepository.findOneBy({id})
    if(!user) throw new NotFoundException('user not found.')
    return user;
  }
  async findUserByEmail(email:string){
    return await this.userRepository.findOneBy({email});
  }
  async accessToken(user:UserEntity): Promise<string>{
    return sign({id:user.id,email:user.email},process.env.ACCESS_TOKEN_SECRET_KEY,{expiresIn:process.env.ACCESS_TOKEN_EXPIRE_TIME})
  }

  async update( userUpdate: UpdateUserDto) {
    //buscar user
    const new_user =await this.findOne(userUpdate.id);
    //no encontrado
    if(!new_user) throw new NotFoundException('user no encontrada');
    //asignando los campos al parcial
    Object.assign(new_user,userUpdate);
    return await this.userRepository.save(new_user);
  }
  
  async remove(id: number) {
    try{
    //buscar user
    const new_user =await this.findOne(id);
    //no encontrado
    if(!new_user) throw new NotFoundException('user no encontrada');
    const remove =await this.userRepository.remove(new_user);
    if(remove) return 'bueno'
    }
    catch(error){
      return 'malo';
    }
  }


}
