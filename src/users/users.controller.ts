import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { UsersService } from './users.service';
import { UserSingUpDto } from './dto/user-singup.dto';
import { UserSingInDto } from './dto/user-singin.dto';
import { UserEntity } from './entities/user.entity';
import { UpdateUserDto } from './dto/user-update.dto';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @MessagePattern('singUp')
  singUp(@Payload() userSingUpDto: UserSingUpDto) {
    return this.usersService.singup(userSingUpDto);
  }

  @MessagePattern('singIn')
  singIn(@Payload() userSingInDto: UserSingInDto) {
    return this.usersService.singin(userSingInDto);
  }
  
  @MessagePattern('User_Update')
  Update(@Payload() userUpdate: UpdateUserDto) {
    return this.usersService.update(userUpdate);
  }

  @MessagePattern('findAllUsers')
  findAll() {
    return this.usersService.findAll();
  }

  @MessagePattern('findOneUser')
  findOne(@Payload() id: number) {
    return this.usersService.findOne(id);
  }
  
  @MessagePattern('removeUser')
  remove(@Payload() id: number) {
    return this.usersService.remove(id);
  }

  //pegado del app.controller

  @MessagePattern({cmd: 'greeting'})
  getGreetingMessage(name: string): string {
    return `Hello ${name}`;
  }

  @MessagePattern({cmd: 'greeting-async'})//busca una funcion en la cola llamada geeting-async
  //esta es una funcion que recibe
  async getGreetingMessageAysnc(name: string): Promise<string> {
    return `tu no metecabrasaramambiche${name} Async`;
  }

  @EventPattern('book-created')
  async handleBookCreatedEvent(data: Record<string, unknown>) {
    console.log(data);
  }

  @EventPattern('proyectox')
  async abc(){
    return console.log('mensaje desde donde se ejecuta el rabbitmq');
  }
  //

}
