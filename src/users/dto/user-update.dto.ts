import { IsNotEmpty } from "class-validator";
import { UserSingUpDto } from "./user-singup.dto";
import { PartialType } from '@nestjs/mapped-types';

export class UpdateUserDto extends PartialType(UserSingUpDto) {
    @IsNotEmpty()
    id: number;
}