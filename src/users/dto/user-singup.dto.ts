import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";
import { UserSingInDto } from "./user-singin.dto";

//esto es para validar las cosas que entran para users
export class UserSingUpDto extends UserSingInDto{
    @IsNotEmpty({message:'el nombre no puede ser vacio'})
    @IsString({message:'el nombre debe ser un string'})
    name:string;
}
//al usar extends obtengo lo de UserSingInDto
//osea, como iba a usar passowrd e email en singin,se creo la clase singin 
//como singup tenia email y password era un copia y pega
//por eso se suso extends para que singUP tenga email y password