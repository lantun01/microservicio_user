import { Roles } from "src/utility/common/user-roles.enum";
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, Timestamp, UpdateDateColumn } from "typeorm";

@Entity('users')
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @Column({unique:true})
    email: string;
    @Column({select:false})//no puede haber contrasena predeterminada
    password: string;
    @Column({type:'enum',enum:Roles,array:true,default:[Roles.ADMIN]})
    roles:Roles[]
    @CreateDateColumn()//esto es pra dejar una marca de tiempo de cuando se creo
    createdAt:Timestamp
    @UpdateDateColumn()
    updateAt:Timestamp
}
