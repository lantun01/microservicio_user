import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls:['amqp://localhost:5672'],
      queue: 'User_queue',//cola por defecto por ahora
      queueOptions: {
        durable: false
      }
    }
  })
  await app.listen();
  console.log('Microservice User is listening');
}
bootstrap()
